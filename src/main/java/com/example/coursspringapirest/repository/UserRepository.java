package com.example.coursspringapirest.repository;

import com.example.coursspringapirest.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;


@Repository
public interface UserRepository extends CrudRepository<User, Integer> {


    User findUserByEmail(String email);




}
