package com.example.coursspringapirest.controller;

import com.example.coursspringapirest.entity.User;
import com.example.coursspringapirest.exception.UserNotFoundException;
import com.example.coursspringapirest.service.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("api/user/")
@Validated
public class FirstApiController {

    /*@GetMapping()
    public String get() {
        return "verbe Get";
    }*/


    @Autowired
    UserService userService;

    @GetMapping()
    public ResponseEntity<String> get() {
        //return ResponseEntity.ok().body("tout est ok");
        return ResponseEntity.status(204).body("tout est ok");
    }

    @GetMapping("get/{id}")
    public ResponseEntity<User> getUser(@PathVariable("id") Integer id) {

        User user = userService.getUserById(id).get();

        if(user==null)
            throw new UserNotFoundException("id:"+id);

        return ResponseEntity.ok(userService.getUserById(id).get());
    }


    @PostMapping("post")
    public ResponseEntity<String> post2(@Valid @RequestBody User user) {

        if (userService.getUserByEmail(user.getEmail())) {
            String errorMessage = "L'utilisateur existe déjà.";
            return ResponseEntity.status(HttpStatus.CONFLICT).body(errorMessage);
        }

        if (user == null || user.getPassword() == null || user.getEmail() == null) {
            String errorMessage = "Requête invalide : le pasword et l'e-mail de l'utilisateur sont requis.";
            return ResponseEntity.badRequest().body(errorMessage);
        }

        // Validation supplémentaire ou traitement pour la création de l'utilisateur

        // Si tout est OK, créer l'utilisateur
        userService.createUser(user);

        // Renvoyer une réponse avec un code de statut 201 (Created) et un message de succès
        return ResponseEntity.status(HttpStatus.CREATED).body("Utilisateur créé avec succès.");
    }


    @GetMapping("/all")
    public ResponseEntity<?> getAllUser() {

        List<User> users = userService.getAllUsers();
        String message = "La liste est vide";
        if (users.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(message);
        } else {
            return ResponseEntity.ok(users);
        }
    }


    @PutMapping("/put")
    public ResponseEntity<String> put(@PathVariable Integer id, @RequestBody User user) {

        if (userService.getUserById(id).isPresent()) {
            String errorMessage = "L'utilisateur avec l'ID " + id + " n'existe pas.";
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorMessage);
        }

        // Vérifier les données de l'utilisateur
        if (user == null || user.getEmail() == null || user.getEmail() == null) {
            String errorMessage = "Requête invalide : le nom et l'e-mail de l'utilisateur sont requis.";
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
        }

        // Mettre à jour les données de l'utilisateur
        userService.updateUser(id, user);

        String successMessage = "Utilisateur avec l'ID " + id + " mis à jour avec succès.";
        return ResponseEntity.ok(successMessage);
    }


    @DeleteMapping("delete/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Integer id, UriComponentsBuilder uriBuilder) {

        if (!userService.getUserById(id).isPresent()) {
            String errorMessage = "L'utilisateur n'existe pas.";
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorMessage);
        }

        // Supprimer l'utilisateur
        userService.deleteUser(id);

        // Redirection vers une autre URL
        //  HttpHeaders headers = new HttpHeaders();
        // headers.setLocation(uriBuilder.path("/user/all").build().toUri());
        // return ResponseEntity.status(HttpStatus.FOUND).headers(headers).build();
        return ResponseEntity.status(HttpStatus.FOUND).location(URI.create("/user/all")).build();
    }
}


